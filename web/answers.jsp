<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:pagination pageCount="${pageCount}" uri="/questions/answers/?t=${param.t}&q=${param.q}">
    <c:set var="isSessionOn" value="${not empty sessionScope.id}"/>
    <c:set var="isAdmin" value="${sessionScope.privilege == 'ADMIN'}"/>
    <table class="table table-striped">
        <thead>
        <tr>
            <td>Topic name</td>
            <td>Message</td>
            <td>Date</td>
        </tr>
        </thead>
        <tbody>
        <tr class="info">
            <td>${question.name}</td>
            <td>${question.message}</td>
            <td>${question.creationDate}</td>
            <td></td>
        </tr>
        </tbody>
    </table>

    <c:if test="${not empty answers}">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>User</td>
                <td>Message</td>
                <td>Date</td>
                <td>
                    <c:if test="${isAuthor || isAdmin}">
                        Action
                    </c:if>
                </td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${answers}" var="answer">
                <c:set var="isAuthor" value="${isSessionOn && (sessionScope.id == answer.author.id)}"/>
                <tr>
                    <td>${answer.author.username}</td>
                    <td>${answer.message}</td>
                    <td>${answer.creationDate}</td>
                    <td>
                        <c:if test="${isAuthor || isAdmin}">
                            <form method="post" action="/questions/answers/DeleteAnswer">
                                <input type="hidden" name="id" value="${answer.id}"/>
                                <input type="submit" value="Delete" class="btn btn-sm btn-danger"/>
                            </form>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <c:if test="${empty answers}">
        <div class="alert alert-info" role="alert">There is no answers.</div>
    </c:if>

    <c:if test="${isSessionOn}">
        <div>
            <form method="post" action="/questions/answers/CreateAnswer">
                Leave an answer: <textarea name="message" required rows="5" cols="40"></textarea>
                <input type="hidden" name="questionId" value="${param.q}"/>
                <input type="hidden" name="topicId" value="${param.t}"/>
                <input type="hidden" name="authorId" value="${sessionScope.id}"/>
                <input type="submit" class="btn btn-xs btn-success"/>
            </form>
        </div>
    </c:if>
</t:pagination>