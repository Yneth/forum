<%@tag description="Pagination template" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="pageCount" required="true" %>
<%@attribute name="uri" required="true" %>

<t:forumpage>
    <jsp:body>
        <jsp:doBody/>
        <c:if test="${pageCount > 0}">
            <ul class="pagination">
                <c:forEach begin="1" end="${pageCount}" var="index">
                    <li class="${(param.p == index) || (param.p == null && index == 1) ? 'active' : ''}">
                        <a href="${uri}&o=${param.o}&p=${index}">${index}</a>
                    </li>
                </c:forEach>
            </ul>
        </c:if>
    </jsp:body>
</t:forumpage>