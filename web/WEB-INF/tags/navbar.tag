<%@tag description="Login template" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
        <li class="active"><a href="/">Home</a></li>
        <c:if test="${not empty param.t && not empty topic}">
            <li><a href="/questions/?t=${param.t}">${topic.name}</a></li>
        </c:if>
        <c:if test="${not empty param.t && not empty question.topic}">
            <li><a href="/questions/?t=${param.t}">${question.topic.name}</a></li>
        </c:if>
        <c:if test="${not empty param.t && not empty question}">
            <li><a href="/questions/answers/?t=${param.t}&q=${param.q}">${question.name}</a></li>
        </c:if>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <c:if test="${empty sessionScope.id}">
            <li><a href="/Register">Register</a></li>
            <li class="dropdown">
                <a href="/" class="dropdown-toggle" data-toggle="dropdown" role="button"
                   aria-expanded="false">Login <span
                        class="caret"></span></a>
                <ul class="dropdown-menu" role="menu" style="min-width: 205px;">
                    <form method="post" action="/Login" class="form-signin">
                        <li><h2 class="form-signin-heading">Please sign in</h2></li>
                        <label for="loginInput" class="sr-only">Username</label>
                        <li><input type="text" name="login" id="loginInput" class="form-control" placeholder="Username"
                                   required
                                   autofocus/></li>
                        <label for="passInput" class="sr-only">Password</label>
                        <li><input type="password" name="password" id="passInput" class="form-control"
                                   placeholder="Password"
                                   required/></li>
                        <li><input type="submit" value="Login" class="btn btn-primary btn-block"/></li>
                    </form>
                </ul>
            </li>
        </c:if>
        <c:if test="${not empty sessionScope.id}">
            <li>
                <form method="post" action="/Logout" class="form-signin">
                    <input type="submit" value="Logout" class="btn btn-link"/>
                </form>
            </li>
        </c:if>
    </ul>
</div>
