<%@tag description="Login template" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<div id="pageheader">
    <div class="container">
        <c:if test="${sessionScope.id == null}">
            <form method="post" action="/Login" class="form-signin">
                <h2 class="form-signin-heading">Please sign in</h2>
                <label for="loginInput" class="sr-only">Username</label>
                <input type="text" name="login" id="loginInput" class="form-control" placeholder="Username" required
                       autofocus/>
                <label for="passInput" class="sr-only">Password</label>
                <input type="password" name="password" id="passInput" class="form-control" placeholder="Password"
                       required/>
                <input type="submit" value="Login" class="btn btn-primary btn-block"/>
            </form>
        </c:if>
        <c:if test="${sessionScope.id != null}">
            <form method="post" action="/Logout" class="form-signin">
                <input type="submit" value="Logout" class="btn btn-block btn-default"/>
            </form>
        </c:if>
    </div>

</div>