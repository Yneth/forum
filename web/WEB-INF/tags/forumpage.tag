<%@tag description="Forum page template" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:genericpage>
    <jsp:attribute name="header">
        <t:navbar/>
        <c:if test="${errorMessage != null}">
            <div class="alert alert-danger" role="alert">
                    <strong>Error: </strong>${errorMessage}
            </div>
        </c:if>
    </jsp:attribute>

    <jsp:attribute name="footer">
        <p></p>
    </jsp:attribute>

    <jsp:body>
        <jsp:doBody/>
    </jsp:body>
</t:genericpage>