<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:forumpage>
    <div class="alert alert-danger" role="alert">
        Requested data not found
    </div>
</t:forumpage>