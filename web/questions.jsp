<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:pagination pageCount="${pageCount}" uri="/questions/?t=${param.t}">
    <c:if test="${not empty questions}">
        <table class="table table-stripped">
            <thead>
            <tr>
                <td>Question</td>
                <td>Answer count</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${questions}" var="question">
                <tr>
                    <td><a href="/questions/answers/?t=${param.t}&q=${question.id}">${question.name}</a></td>
                    <td>${question.answersCount}</td>
                    <td>
                        <c:if test="${sessionScope.privilege == 'ADMIN' || sessionScope.id == question.author.id}">
                            <form method="post" action="/questions/DeleteQuestion">
                                <input type="hidden" name="id" value="${question.id}"/>
                                <input type="submit" value="Delete" class="btn btn-sm btn-danger"/>
                            </form>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <c:if test="${empty questions}">
        <div class="alert alert-info" role="alert">There is no questions.</div>
    </c:if>
    <c:if test="${not empty sessionScope.id}">
        <div>
            Add question:
            <form method="post" action="/questions/CreateQuestion">
                Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" name="name"/></br>
                Message: <textarea name="message" required rows="5" cols="40"></textarea>
                <input type="hidden" name="topicId" value="${param.t}"/>
                <input type="hidden" name="authorId" value="${sessionScope.id}"/><%--REMOVE THIS--%>
                <input type="submit" class="btn btn-xs btn-success"/>
            </form>
        </div>
    </c:if>
</t:pagination>