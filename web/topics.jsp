<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:pagination pageCount="${pageCount}" uri="/?">
    <c:if test="${not empty topics}">
    <table class="table table-striped">
        <thead>
        <tr>
            <td>Topics</td>
            <td>Question count</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${topics}" var="topic">
            <tr>
                <td>
                    <a href="/questions/?t=${topic.id}">${topic.name}</a></td>
                <td>${topic.questionCount}</td>
                <c:if test="${sessionScope.privilege == 'ADMIN'}">
                    <td>
                        <form method="post" action="/DeleteTopic">
                            <input type="hidden" name="id" value="${topic.id}"/>
                            <input type="submit" value="delete" class="btn btn-sm btn-danger"/>
                        </form>
                    </td>
                </c:if>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </c:if>
    <c:if test="${empty topics}">
        <div class="alert alert-info" role="alert">There is no topics yet.</div>
    </c:if>
    <c:if test="${not empty sessionScope.id && sessionScope.privilege == 'ADMIN'}">
        <div>
            Add topic:
            <form method="post" action="/CreateTopic/">
                <input type="text" name="name"/>
                <input type="submit" class="btn btn-xs btn-success"/>
            </form>
        </div>
    </c:if>
</t:pagination>