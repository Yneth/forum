<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:forumpage>
    <c:if test="${not empty sessionScope.id}">
        <div>Your cabinet</div>
    </c:if>
    <c:if test="${empty sessionScope.id}">
        <div>You need to <a href="/"></a> or <a href="/Register">register</a></div>
    </c:if>
</t:forumpage>