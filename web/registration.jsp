<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:forumpage>
    <div class="container">
        <c:if test="${sessionScope.id == null}">
            <form method="post" action="/Register" class="form-signin">
                <h2 class="form-signin-heading">Register</h2>
                <label for="loginInput" class="sr-only">Username</label>
                <input type="text" name="login" id="loginInput" class="form-control" placeholder="Username" required
                       autofocus/>
                <label for="passInput" class="sr-only">Password</label>
                <input type="password" name="password" id="passInput" class="form-control" placeholder="Password"
                       required/>
                <label for="firstName" class="sr-only">First name</label>
                <input type="text" name="firstName" id="firstName" class="form-control" placeholder="First name"
                       required/>
                <label for="secondName" class="sr-only">Second name</label>
                <input type="text" name="secondName" id="secondName" class="form-control" placeholder="Second name"
                       required/>
                <input type="submit" value="Sign up" class="btn btn-primary btn-block"/>
            </form>
        </c:if>
    </div>
</t:forumpage>