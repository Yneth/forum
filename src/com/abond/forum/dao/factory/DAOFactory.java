package com.abond.forum.dao.factory;

import com.abond.forum.dao.base.AnswerDAO;
import com.abond.forum.dao.base.QuestionDAO;
import com.abond.forum.dao.base.TopicDAO;
import com.abond.forum.dao.base.UserDAO;

import javax.sql.DataSource;

public abstract class DAOFactory {
    protected DataSource dataSource;

    public DAOFactory(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public abstract UserDAO getUserDAO();
    public abstract TopicDAO getTopicDAO();
    public abstract QuestionDAO getQuestionDAO();
    public abstract AnswerDAO getAnswerDAO();
}
