package com.abond.forum.dao.factory;

import com.abond.forum.dao.base.AnswerDAO;
import com.abond.forum.dao.base.QuestionDAO;
import com.abond.forum.dao.base.TopicDAO;
import com.abond.forum.dao.oracle.OracleAnswerDAO;
import com.abond.forum.dao.oracle.OracleQuestionDAO;
import com.abond.forum.dao.oracle.OracleTopicDAO;
import com.abond.forum.dao.oracle.OracleUserDAO;
import com.abond.forum.dao.base.UserDAO;

import javax.sql.DataSource;
import java.util.logging.Logger;

public class OracleDAOFactory extends DAOFactory {
    private static final Logger logger = Logger.getLogger(OracleDAOFactory.class.getSimpleName());

    public OracleDAOFactory(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public UserDAO getUserDAO() {
        return new OracleUserDAO(dataSource);
    }

    @Override
    public TopicDAO getTopicDAO() {
        return new OracleTopicDAO(dataSource);
    }

    @Override
    public QuestionDAO getQuestionDAO() {
        return new OracleQuestionDAO(dataSource);
    }

    @Override
    public AnswerDAO getAnswerDAO() {
        return new OracleAnswerDAO(dataSource);
    }

//    public static Connection getConnection() throws ServletException {
//        try {
//            Connection conn = dataSource.getConnection();
//            conn.setAutoCommit(false);
//            return conn;
//        } catch (SQLException ex) {
//            LogUtils.logSevere(logger, ex);
//            throw new ServletException(
//                    "Cannot obtain connection", ex);
//        }
//    }
//
//    public void releaseConnection(final Connection connection)
//            throws ServletException {
//        try {
//            if (connection != null)
//                connection.close();
//        } catch (SQLException ex) {
//            LogUtils.logSevere(logger, ex);
//            throw new ServletException(
//                    "Cannot release connection", ex);
//        }
//    }
//
//    private DataSource initDataSource() {
//        DataSource tempDataSource = null;
//        try {
//            Context ic = new InitialContext();
//            tempDataSource = (DataSource) ic.lookup("java:comp/env/oracle");
//        } catch (NamingException e) {
//            logger.severe("OracleDAO: failed to initialize DataSource.");
//            LogUtils.logSevere(logger, e);
//        }
//        return tempDataSource;
//    }
}
