package com.abond.forum.dao.oracle;

import com.abond.forum.entity.impl.Topic;
import com.abond.forum.entity.impl.User;
import com.abond.forum.util.LogUtils;
import com.abond.forum.dao.base.AbstractDAO;
import com.abond.forum.dao.base.QuestionDAO;
import com.abond.forum.entity.impl.Question;
import com.abond.forum.util.DAOUtils;

import javax.servlet.ServletException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OracleQuestionDAO extends AbstractDAO<Question> implements QuestionDAO {
    private static final Logger logger = Logger.getLogger(OracleQuestionDAO.class.getSimpleName());

    //region QUERY_STRINGS
    private static final String FIND_BY_ID =
            "SELECT q.QUESTION_ID, q.NAME, q.MESSAGE, q.CREATION_DATE, q.AUTHOR_ID, q.TOPIC_ID, t.NAME\n" +
                    "FROM QUESTION q\n" +
                    "INNER JOIN TOPIC t ON q.TOPIC_ID = t.TOPIC_ID\n" +
                    "WHERE q.QUESTION_ID = ?";

    private static final String INSERT =
            "INSERT INTO QUESTION VALUES(NULL, ?, ?, ?, ?, ?)";

    private static final String UPDATE_BY_ID =
            "UPDATE QUESTION SET NAME = ?, MESSAGE = ? \n" +
                    "WHERE QUESTION_ID = ?";

    private static final String DELETE_BY_ID =
            "DELETE FROM QUESTION\n" +
                    "WHERE QUESTION_ID = ?";

    private static final String PAGINATION =
            "SELECT \n" +
                    "q.QUESTION_ID, q.NAME, \n" +
                    "q.CREATION_DATE, q.AUTHOR_ID, \n" +
                    "q.TOPIC_ID, u.USERNAME, COUNT(a.ANSWER_ID)\n" +
                    "FROM (\n" +
                    "  SELECT QUESTION_ID, NAME, CREATION_DATE, AUTHOR_ID, TOPIC_ID, ROWNUM rn\n" +
                    "  FROM (\n" +
                    "    SELECT QUESTION_ID, NAME, CREATION_DATE, AUTHOR_ID, TOPIC_ID\n" +
                    "    FROM QUESTION\n" +
                    "    ORDER BY CREATION_DATE\n" +
                    "  ) WHERE ROWNUM <= ? AND TOPIC_ID = ?\n" +
                    ") q\n" +
                    "INNER JOIN FORUM_USER u ON q.AUTHOR_ID = u.USER_ID\n" +
                    "LEFT JOIN ANSWER a ON q.QUESTION_ID = a.QUESTION_ID\n" +
                    "WHERE rn > ?\n" +
                    "GROUP BY\n" +
                    "  q.QUESTION_ID, q.NAME,\n" +
                    "  q.CREATION_DATE, q.AUTHOR_ID,\n" +
                    "  q.TOPIC_ID, u.USERNAME";

    private static final String COUNT =
            "SELECT COUNT(QUESTION_ID) \n" +
                    "FROM QUESTION\n" +
                    "WHERE TOPIC_ID = ?";
    //endregion

    private Connection connection;

    public OracleQuestionDAO(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public Question findById(Integer id) throws ServletException {
        Question question = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(FIND_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int questionId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String message = resultSet.getString(3);
                Date creationDate = resultSet.getDate(4);
                int authorId = resultSet.getInt(5);
                int topicId = resultSet.getInt(6);
                String topicName = resultSet.getString(7);
                User user = new User(authorId);
                Topic topic = new Topic(topicId, topicName);
                question = new Question(questionId, creationDate, name, message, topic, user);
            }
        } catch (ServletException e) {
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
        return question;
    }

    @Override
    public void save(Question question) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setDate(1, question.getCreationDate());
            preparedStatement.setString(2, question.getName());
            preparedStatement.setString(3, question.getMessage());
            preparedStatement.setInt(4, question.getAuthor().getId());
            preparedStatement.setInt(5, question.getTopic().getId());

            if (preparedStatement.executeUpdate() <= 0) {
                logger.severe("Question.save(a) affected more/less than 1 row.");
            }
            connection.commit();
        } catch (ServletException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public void deleteById(Integer id) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(DELETE_BY_ID);
            preparedStatement.setInt(1, id);
            if (preparedStatement.executeUpdate() <= 0) {
                logger.severe("Question.delete(a) affected more/less than 1 row.");
            }
            connection.commit();
        } catch (ServletException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public void update(Question question) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(UPDATE_BY_ID);
            preparedStatement.setString(1, question.getName());
            preparedStatement.setString(2, question.getMessage());
            if (preparedStatement.executeUpdate() <= 0) {
                logger.severe("Question.update(a) affected more/less than 1 row.");
            }
            connection.commit();
        } catch (ServletException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public Collection<Question> getPaginatedEntities(int from, int to) throws ServletException {
        PreparedStatement preparedStatement = null;
        Collection<Question> questions = new ArrayList<Question>();
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(PAGINATION);
            preparedStatement.setInt(1, to);
            preparedStatement.setInt(2, from);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int questionId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                Date creationDate = resultSet.getDate(3);
                int authorId = resultSet.getInt(4);
                int topicId = resultSet.getInt(5);
                User author = new User(authorId);
                Topic topic = new Topic(topicId);
                questions.add(new Question(questionId, creationDate, name, "", topic, author));
            }
        } catch (ServletException e) {
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
        return questions;
    }

    @Override
    public Collection<Question> getPaginatedToRelatedTopic(int from, int to, int toTopicId)
            throws ServletException {
        PreparedStatement preparedStatement = null;
        Collection<Question> questions = new ArrayList<Question>();
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(PAGINATION);
            preparedStatement.setInt(1, to);
            preparedStatement.setInt(2, toTopicId);
            preparedStatement.setInt(3, from);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int questionId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                Date creationDate = resultSet.getDate(3);
                int authorId = resultSet.getInt(4);
                int topicId = resultSet.getInt(5);
                int answersCount = resultSet.getInt(7);
                User user = new User(authorId);
                Topic topic = new Topic(topicId);
                questions.add(new Question(questionId, creationDate, name, "", topic, user, answersCount));
            }
        } catch (ServletException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            e.printStackTrace();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            e.printStackTrace();
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
        return questions;
    }

    @Override
    public int getCountByTopicId(int topicId) throws ServletException, SQLException {
        int result = 0;
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(COUNT);
            preparedStatement.setInt(1, topicId);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next())
                result = rs.getInt(1);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseStatement(preparedStatement, logger);
            DAOUtils.releaseConnection(connection, logger);
        }
        return result;
    }
}
