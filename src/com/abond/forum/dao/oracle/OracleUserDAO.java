package com.abond.forum.dao.oracle;

import com.abond.forum.entity.impl.User;
import com.abond.forum.util.LogUtils;
import com.abond.forum.dao.base.AbstractDAO;
import com.abond.forum.dao.base.UserDAO;
import com.abond.forum.entity.impl.PrivilegeType;
import com.abond.forum.util.DAOUtils;

import javax.servlet.ServletException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.Collection;
import java.util.logging.Logger;

public class OracleUserDAO extends AbstractDAO<User> implements UserDAO {
    private static final Logger logger = Logger.getLogger(OracleUserDAO.class.getSimpleName());

    //region QUERY_STRINGS
    private static final String FIND_BY_ID =
            "SELECT DISTINCT USER_ID, CREATION_DATE, USERNAME, PASSWORD, FIRST_NAME, SECOND_NAME, PRIVILEGE_ID\n" +
                    "FROM FORUM_USER\n" +
                    "WHERE USER_ID = ?";

    private static final String FIND_BY_LOG_PASS =
            "SELECT USER_ID, CREATION_DATE, USERNAME, PASSWORD, FIRST_NAME, SECOND_NAME, PRIVILEGE_ID\n" +
                    "FROM FORUM_USER\n" +
                    "WHERE USERNAME = ? AND PASSWORD = ?";

    private static final String INSERT =
            "INSERT INTO FORUM_USER VALUES (NULL, ?, ?, ?, ?, ?, ?)";

    private static final String UPDATE_BY_ID =
            "UPDATE FORUM_USER SET PASSWORD = ?, FIRST_NAME = ?, SECOND_NAME = ?, PRIVILEGE = ?\n" +
                    "WHERE USER_ID = ?";

    private static final String DELETE_BY_ID =
            "DELETE FORUM_USER WHERE USER_ID = ?";

    private static final String PAGINATION =
            "SELECT USER_ID, CREATION_DATE, USERNAME, FIRST_NAME, SECOND_NAME\n" +
                    "FROM (\n" +
                    "SELECT USER_ID, CREATION_DATE, USERNAME, FIRST_NAME, SECOND_NAME, ROWNUM rn\n" +
                    "FROM (\n" +
                    "SELECT USER_ID, CREATION_DATE, USERNAME, FIRST_NAME, SECOND_NAME\n" +
                    "FROM FORUM_USER\n" +
                    ") WHERE ROWNUM < ?\n" +
                    ") WHERE rn >= ?";
    //endregion

    private Connection connection;

    public OracleUserDAO(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public User findById(Integer id) {
        return null;
    }

    @Override
    public void save(User user) throws SQLException, ServletException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setDate(1, user.getCreationDate());
            preparedStatement.setString(2, user.getUsername());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getFirstname());
            preparedStatement.setString(5, user.getSecondname());
            preparedStatement.setInt(6, user.getPrivilege().getType());
            if (preparedStatement.executeUpdate() <= 0) {
                logger.severe("User.save(u) affected more/less than 1 row.");
            }
            connection.commit();
        } catch (ServletException e) {
            LogUtils.logSevere(logger, e);
            connection.rollback();
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
            connection.rollback();
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public void deleteById(Integer id) {

    }

    @Override
    public void update(User user) {

    }

    @Override
    public Collection<User> getPaginatedEntities(int from, int to) {
        return null;
    }


    @Override
    public User findByLoginAndPassword(String login, String password) throws ServletException {
        User user = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(FIND_BY_LOG_PASS);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int userId = resultSet.getInt(1);
                Date creationDate = resultSet.getDate(2);
                String userLogin = resultSet.getString(3);
                String userPass = resultSet.getString(4);
                String firstName = resultSet.getString(5);
                String secondName = resultSet.getString(6);
                int privilege = resultSet.getInt(7);
                user = new User(
                        userId, creationDate, userLogin, password, firstName, secondName, PrivilegeType.byId(privilege)
                );
            }
        } catch (ServletException e) {
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
        return user;
    }
}
