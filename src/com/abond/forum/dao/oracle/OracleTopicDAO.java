package com.abond.forum.dao.oracle;

import com.abond.forum.dao.base.TopicDAO;
import com.abond.forum.entity.impl.Topic;
import com.abond.forum.dao.base.AbstractDAO;
import com.abond.forum.util.DAOUtils;
import com.abond.forum.util.LogUtils;

import javax.servlet.ServletException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;

public class OracleTopicDAO extends AbstractDAO<Topic> implements TopicDAO {
    private static final Logger logger = Logger.getLogger(OracleTopicDAO.class.getSimpleName());

    private Connection connection;

    //region QUERY STRINGS
    private static final String FIND_BY_ID =
            "SELECT TOPIC_ID, CREATION_DATE, NAME\n" +
                    "FROM TOPIC\n" +
                    "WHERE TOPIC_ID = ?";

    private static final String INSERT = "INSERT INTO TOPIC VALUES(NULL, ?, ?)";

    private static final String UPDATE_BY_ID =
            "UPDATE TOPIC SET name = ? " +
            "WHERE topic_id = ?";

    private static final String DELETE_BY_ID =
            "DELETE FROM TOPIC " +
            "WHERE TOPIC_ID = ?";

    private static final String PAGINATION =
            "SELECT t.topic_id, t.name, t.creation_date, COUNT(q.topic_id) AS QUESTION_COUNT\n" +
            "FROM (\n" +
            "    SELECT topic_id, name, creation_date, ROWNUM rn\n" +
            "    FROM ( \n" +
            "        SELECT topic_id, name, creation_date\n" +
            "        FROM topic\n" +
            "        ORDER BY creation_date\n" +
            "    ) WHERE ROWNUM <= ?\n" +
            ") t\n" +
            "LEFT JOIN QUESTION q ON q.TOPIC_ID = t.TOPIC_ID\n" +
            "WHERE rn > ?\n" +
            "GROUP BY\n" +
            "  t.topic_id,\n" +
            "  t.name,\n" +
            "  t.creation_date";
    private static final String COUNT =
            "SELECT COUNT(TOPIC_ID)\n" +
                    "FROM TOPIC";

    public OracleTopicDAO(DataSource dataSource) {
        super(dataSource);
    }
    //endregion

    @Override
    public Topic findById(Integer id) throws ServletException {
        Topic topic = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(FIND_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int topicId = resultSet.getInt(1);
                Date creationDate = resultSet.getDate(2);
                String name = resultSet.getString(3);
                topic = new Topic(topicId, creationDate, name);
            }
        } catch (ServletException e) {
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
        return topic;
    }

    @Override
    public void save(Topic topic) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(INSERT);
//            preparedStatement.setInt(1, topic.getId());
            preparedStatement.setString(1, topic.getName());
            preparedStatement.setDate(2, topic.getCreationDate());
            if (preparedStatement.executeUpdate() != 1) {
                logger.severe("Answer.save(a) affected more/less than 1 row.");
            }
            connection.commit();
        } catch (ServletException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public void deleteById(Integer id) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(DELETE_BY_ID);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            connection.commit();
        } catch (ServletException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public void update(Topic topic) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(UPDATE_BY_ID);
            preparedStatement.setString(1, topic.getName());
            preparedStatement.setInt(2, topic.getId());
            if (preparedStatement.executeUpdate() <= 0) {
                logger.severe("Answer.update(a) affected more/less than 1 row.");
            }
            connection.commit();
        } catch (ServletException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public Collection<Topic> getPaginatedEntities(int from, int to) throws ServletException {
        PreparedStatement preparedStatement = null;
        Collection<Topic> topics = new ArrayList<Topic>();
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(PAGINATION);
            preparedStatement.setInt(1, to);
            preparedStatement.setInt(2, from);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int topicId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                Date creationDate = resultSet.getDate(3);
                int questionCount = resultSet.getInt(4);
                topics.add(new Topic(topicId, creationDate, name, questionCount));
            }
        } catch (ServletException e) {
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
        return topics;
    }

    @Override
    public int getCount() throws ServletException, SQLException {
        int result = 0;
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(COUNT);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next())
                result = rs.getInt(1);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseStatement(preparedStatement, logger);
            DAOUtils.releaseConnection(connection, logger);
        }
        return result;
    }
}
