package com.abond.forum.dao.oracle;

import com.abond.forum.dao.base.AnswerDAO;
import com.abond.forum.entity.impl.User;
import com.abond.forum.util.LogUtils;
import com.abond.forum.dao.base.AbstractDAO;
import com.abond.forum.entity.impl.Answer;
import com.abond.forum.entity.impl.Question;
import com.abond.forum.util.DAOUtils;

import javax.servlet.ServletException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OracleAnswerDAO extends AbstractDAO<Answer> implements AnswerDAO {
    private static final Logger logger = Logger.getLogger(OracleAnswerDAO.class.getSimpleName());

    //region QUERY STRINGS
    private static final String FIND_BY_ID =
            "SELECT DISTINCT a.ANSWER_ID, a.MESSAGE, a.CREATION_DATE, a.AUTHOR_ID, a.QUESTION_ID, u.USERNAME\n" +
            "FROM ANSWER a\n" +
            "INNER JOIN FORUM_USER u ON a.AUTHOR_ID = u.USER_ID\n" +
            "WHERE a.ANSWER_ID = ?";

    private static final String SAVE = "INSERT INTO ANSWER VALUES(NULL, ?, ?, ?, ?)";

    private static final String UPDATE_BY_ID = "UPDATE ANSWER SET MESSAGE = ? WHERE ANSWER_ID = ?";

    private static final String DELETE_BY_ID = "DELETE FROM ANSWER WHERE ANSWER_ID = ?";

    private static final String PAGINATION =
            "SELECT a.ANSWER_ID, a.CREATION_DATE, a.MESSAGE, a.AUTHOR_ID, a.QUESTION_ID, u.USERNAME\n" +
                "FROM (\n" +
                    "SELECT ANSWER_ID, CREATION_DATE, MESSAGE, AUTHOR_ID, QUESTION_ID, ROWNUM rn\n" +
                    "FROM (\n" +
                        "SELECT ANSWER_ID, CREATION_DATE, MESSAGE, AUTHOR_ID, QUESTION_ID\n" +
                        "FROM ANSWER\n" +
                        "ORDER BY CREATION_DATE\n" +
                    ") WHERE ROWNUM <= ? AND QUESTION_ID = ?\n" +
                ") a\n" +
                "INNER JOIN FORUM_USER u ON a.AUTHOR_ID = u.USER_ID\n" +
                "WHERE rn > ?";
    private static final String COUNT =
            "SELECT COUNT(ANSWER_ID) \n" +
                    "FROM ANSWER\n" +
                    "WHERE QUESTION_ID = ?";
    //endregion

    private Connection connection;

    public OracleAnswerDAO(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public Answer findById(Integer id) throws ServletException {
        Answer answer = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(FIND_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int answerId = resultSet.getInt(1);
                String message = resultSet.getString(2);
                Date creationDate = resultSet.getDate(3);
                int authorId = resultSet.getInt(4);
                int questionId = resultSet.getInt(5);
                String userName = resultSet.getString(6);
                User user = new User(authorId, userName);
                Question question = new Question(questionId);
                answer = new Answer(answerId, creationDate, message, question, user);
            }
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
        return answer;
    }

    @Override
    public void save(Answer answer) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(SAVE);
//            preparedStatement.setInt(1, answer.getId());
            preparedStatement.setDate(1, answer.getCreationDate());
            preparedStatement.setString(2, answer.getMessage());
            preparedStatement.setInt(3, answer.getAuthor().getId());
            preparedStatement.setInt(4, answer.getQuestion().getId());
            if (preparedStatement.executeUpdate() <= 0) {
                logger.severe("Answer.save(a) affected more/less than 1 row.");
            }
            connection.commit();
        } catch (ServletException e) {
            LogUtils.logSevere(logger, e);
            connection.rollback();
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
            connection.rollback();
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public void deleteById(Integer id) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(DELETE_BY_ID);
            preparedStatement.setInt(1, id);
            if (preparedStatement.executeUpdate() <= 0) {
                logger.severe("Answer.delete(a) affected more/less than 1 row.");
            }
            connection.commit();
        } catch (ServletException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public void update(Answer answer) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(UPDATE_BY_ID);
            preparedStatement.setString(1, answer.getMessage());
            preparedStatement.setInt(2, answer.getId());
            if (preparedStatement.executeUpdate() <= 0) {
                logger.severe("Answer.update(a) affected more/less than 1 row.");
            }
            connection.commit();
        } catch (ServletException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
    }

    @Override
    public Collection<Answer> getPaginatedEntities(int from, int to) throws ServletException, SQLException {
        PreparedStatement preparedStatement = null;
        Collection<Answer> answers = new ArrayList<Answer>();
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(PAGINATION);
            preparedStatement.setInt(1, to);
            preparedStatement.setInt(2, from);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int answerId = resultSet.getInt(1);
                String message = resultSet.getString(2);
                Date creationDate = resultSet.getDate(3);
                int authorId = resultSet.getInt(4);
                int questionId = resultSet.getInt(5);
                String userName = resultSet.getString(6);
                User author = new User(authorId, userName);
                Question question = new Question(questionId);
                answers.add(new Answer(answerId, creationDate, message, question, author));
            }
        } catch (ServletException e) {
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
        return answers;
    }

    @Override
    public Collection<Answer> getPaginatedRelatedToQuestion(int from, int to, int toQuestionId)
            throws ServletException {
        PreparedStatement preparedStatement = null;
        Collection<Answer> answers = new ArrayList<Answer>();
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(PAGINATION);
            preparedStatement.setInt(1, to);
            preparedStatement.setInt(2, toQuestionId);
            preparedStatement.setInt(3, from);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int answerId = resultSet.getInt(1);
                Date creationDate = resultSet.getDate(2);
                String message = resultSet.getString(3);
                int authorId = resultSet.getInt(4);
                int questionId = resultSet.getInt(5);
                String userName = resultSet.getString(6);
                User author = new User(authorId, userName);
                Question question = new Question(questionId);
                answers.add(new Answer(answerId, creationDate, message, question, author));
            }
        } catch (ServletException e) {
            LogUtils.logSevere(logger, e);
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseConnection(connection, logger);
            DAOUtils.releaseStatement(preparedStatement, logger);
        }
        return answers;
    }

    @Override
    public int getCountByQuestionId(int questionId) throws ServletException, SQLException {
        int result = 0;
        PreparedStatement preparedStatement = null;
        try {
            connection = DAOUtils.getConnection(dataSource, logger);
            preparedStatement = connection.prepareStatement(COUNT);
            preparedStatement.setInt(1, questionId);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next())
                result = rs.getInt(1);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            LogUtils.logSevere(logger, e);
        } finally {
            DAOUtils.releaseStatement(preparedStatement, logger);
            DAOUtils.releaseConnection(connection, logger);
        }
        return result;
    }
}
