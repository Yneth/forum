package com.abond.forum.dao.base;

import javax.servlet.ServletException;
import java.sql.SQLException;
import java.util.Collection;

public interface DAO<T> {
    T findById(Integer id) throws ServletException;
    void save(T t) throws ServletException, SQLException;
    void deleteById(Integer id) throws ServletException, SQLException;
    void update(T t) throws ServletException, SQLException;
    Collection<T> getPaginatedEntities(final int from, final int to) throws ServletException, SQLException;
}
