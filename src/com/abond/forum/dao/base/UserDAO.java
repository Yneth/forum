package com.abond.forum.dao.base;

import com.abond.forum.entity.impl.User;

import javax.servlet.ServletException;

public interface UserDAO extends DAO<User> {
    User findByLoginAndPassword(String login, String password) throws ServletException;
}
