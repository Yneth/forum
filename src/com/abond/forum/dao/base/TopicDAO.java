package com.abond.forum.dao.base;

import com.abond.forum.entity.impl.Topic;

import javax.servlet.ServletException;
import java.sql.SQLException;

public interface TopicDAO extends DAO<Topic> {
    int getCount() throws ServletException, SQLException;
}
