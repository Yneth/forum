package com.abond.forum.dao.base;

import com.abond.forum.entity.impl.Answer;

import javax.servlet.ServletException;
import java.sql.SQLException;
import java.util.Collection;

public interface AnswerDAO extends DAO<Answer> {
    Collection<Answer> getPaginatedRelatedToQuestion(int from, int to, int pageId) throws ServletException;
    int getCountByQuestionId(int questionId) throws ServletException, SQLException;
}
