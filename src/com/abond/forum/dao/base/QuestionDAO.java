package com.abond.forum.dao.base;

import com.abond.forum.entity.impl.Question;

import javax.servlet.ServletException;
import java.sql.SQLException;
import java.util.Collection;

public interface QuestionDAO extends DAO<Question> {
    Collection<Question> getPaginatedToRelatedTopic(int from, int to, int toTopicId)
            throws ServletException;
    int getCountByTopicId(int topicId) throws ServletException, SQLException;
}
