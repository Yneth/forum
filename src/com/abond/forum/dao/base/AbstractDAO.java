package com.abond.forum.dao.base;

import javax.sql.DataSource;

public abstract class AbstractDAO<T> implements DAO<T> {
    protected DataSource dataSource;

    public AbstractDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
