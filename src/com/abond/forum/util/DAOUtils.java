package com.abond.forum.util;

import javax.servlet.ServletException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

public class DAOUtils {

    public static void releaseConnection(final Connection connection, final Logger logger)
            throws ServletException {
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException ex) {
            LogUtils.logSevere(logger, ex);
            throw new ServletException(
                    "Cannot release connection", ex);
        }
    }

    public static void releaseStatement(final PreparedStatement preparedStatement, Logger logger) {
        try {
            if (preparedStatement != null)
                preparedStatement.close();
        } catch (SQLException e) {
            LogUtils.logSevere(logger, e);
        }
    }

    public static Connection getConnection(final DataSource dataSource, final Logger logger)
            throws ServletException {
        try {
            Connection conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            return conn;
        } catch (SQLException ex) {
            LogUtils.logSevere(logger, ex);
            throw new ServletException(
                    "Cannot obtain connection", ex);
        }
    }
}
