package com.abond.forum.util;

public class PaginationUtils {
    public static int getPageCount(int recordCount, double offset) {
        int pageCount = ceil(recordCount / (offset * 1.0));
        if (pageCount == 0)
            pageCount++;
        return pageCount;
    }

    public static int ceil(double a) {
        int b = (int) a;
        if (b == a)
            return b;
        return b + 1;
    }
}
