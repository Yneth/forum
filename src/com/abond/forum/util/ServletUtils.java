package com.abond.forum.util;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;

public class ServletUtils {
    private static final Logger LOGGER = Logger.getLogger(ServletUtils.class.getSimpleName());
    private static final String EMPTY_STRING = "";

    public static int getIntegerParameter(HttpServletRequest req, String paramName, int defaultValue) {
        int result = defaultValue;
        String stringParameter = req.getParameter(paramName);
        try {
            int value = 0;
            if (stringParameter != null)
                value = Integer.parseInt(stringParameter);
            if (value > 0)
                result = value;
        } catch (NumberFormatException e) {
            LogUtils.logSevere(LOGGER, e);
            LogUtils.logSevere(LOGGER, e);
        }
        return result;
    }

    public static String getStringParameter(HttpServletRequest req, String name) {
        String value = req.getParameter(name);
        return name == null ? EMPTY_STRING : value;
    }
}
