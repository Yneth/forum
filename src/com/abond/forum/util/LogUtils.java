package com.abond.forum.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogUtils {

    public static void logSevere(final Logger logger, Throwable throwable) {
        logger.log(Level.SEVERE, throwable.getMessage(), throwable);
    }
}
