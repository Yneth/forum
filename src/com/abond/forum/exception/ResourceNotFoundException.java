package com.abond.forum.exception;

/**
 * Created by Anton on 24.03.2015.
 */
public class ResourceNotFoundException extends Exception {
    public ResourceNotFoundException() {
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
