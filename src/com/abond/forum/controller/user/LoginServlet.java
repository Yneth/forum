package com.abond.forum.controller.user;

import com.abond.forum.dao.base.UserDAO;
import com.abond.forum.entity.impl.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(LoginServlet.class.getSimpleName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDAO userDao = ((UserDAO) getServletContext().getAttribute("userDao"));

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        User user = userDao.findByLoginAndPassword(login, password);
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
            session = request.getSession();
        }
        if (user != null) {
            session.setAttribute("id", user.getId());
            session.setAttribute("privilege", user.getPrivilege());
        } else {
            session.setAttribute("errorMessage", "Wrong login or password");
        }
        response.sendRedirect("/");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
