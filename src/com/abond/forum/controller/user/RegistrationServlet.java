package com.abond.forum.controller.user;

import com.abond.forum.dao.base.UserDAO;
import com.abond.forum.entity.impl.PrivilegeType;
import com.abond.forum.entity.impl.User;
import com.abond.forum.util.LogUtils;
import com.abond.forum.util.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Logger;

public class RegistrationServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(RegistrationServlet.class.getSimpleName());
    private static final PrivilegeType DEFAULT_PRIVILEGE = PrivilegeType.USER;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDAO userDAO = ((UserDAO) getServletContext().getAttribute("userDAO"));

        HttpSession session = request.getSession(false);
        if (session.getAttribute("id") != null)
            response.sendRedirect("/");

        try {
           userDAO.save(createUser(request));
        } catch (SQLException e) {
            LogUtils.logSevere(LOGGER, e);
            session = request.getSession();
            session.setAttribute("errorMessage", "Could not register your account, try again");
        }
        response.sendRedirect("/");
    }

    private User createUser(HttpServletRequest request) {
        String login = ServletUtils.getStringParameter(request, "login");
        String password = ServletUtils.getStringParameter(request, "password");
        String firstName = ServletUtils.getStringParameter(request, "firstName");
        String secondName = ServletUtils.getStringParameter(request, "secondName");
        Date creationDate = new Date(new java.util.Date().getTime());
        return new User(null, creationDate, login, password, firstName, secondName, DEFAULT_PRIVILEGE);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/registration.jsp").forward(request, response);
    }
}
