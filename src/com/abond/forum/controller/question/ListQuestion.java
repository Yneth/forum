package com.abond.forum.controller.question;

import com.abond.forum.controller.base.ListController;
import com.abond.forum.dao.base.QuestionDAO;
import com.abond.forum.dao.base.TopicDAO;
import com.abond.forum.entity.impl.Question;
import com.abond.forum.entity.impl.Topic;
import com.abond.forum.exception.ResourceNotFoundException;
import com.abond.forum.util.LogUtils;
import com.abond.forum.util.PaginationUtils;
import com.abond.forum.util.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

@WebServlet(name = "QuestionList")
public class ListQuestion extends ListController {

    @Override
    protected void doProcessRequest(HttpServletRequest request, HttpServletResponse response, int from, int to)
            throws IOException, ResourceNotFoundException {
        TopicDAO topicDAO = (TopicDAO) getServletContext().getAttribute("topicDAO");
        QuestionDAO questionDAO = (QuestionDAO) getServletContext().getAttribute("questionDAO");

        int topicId = ServletUtils.getIntegerParameter(request, "t", -1);

        Collection<Question> questions = null;
        Topic currentTopic = null;
        try {
            questions = questionDAO.getPaginatedToRelatedTopic(from, to, topicId);
            currentTopic = topicDAO.findById(topicId);
        } catch (ServletException e) {
            LogUtils.logSevere(LOGGER, e);
        }
        if (currentTopic == null) {
            throw new ResourceNotFoundException();
        }

        request.setAttribute("topic", currentTopic);
        request.setAttribute("questions", questions);
    }

    @Override
    public String getResultPath() {
        return "/questions.jsp";
    }

    @Override
    public int getPageCount(HttpServletRequest request, int offset) {
        QuestionDAO questionDAO = (QuestionDAO) getServletContext().getAttribute("questionDAO");

        int topicId = ServletUtils.getIntegerParameter(request, "t", -1);

        int pageCount = 0;
        try {
            pageCount = PaginationUtils.getPageCount(questionDAO.getCountByTopicId(topicId), offset);
        } catch (SQLException e) {
            LogUtils.logSevere(LOGGER, e);
        } catch (ServletException e) {
            LogUtils.logSevere(LOGGER, e);
        }
        return pageCount;
    }
}
