package com.abond.forum.controller.question;

import com.abond.forum.dao.base.DAO;
import com.abond.forum.controller.base.DeleteServlet;
import com.abond.forum.dao.factory.DAOFactory;
import com.abond.forum.entity.impl.Question;

import javax.servlet.http.HttpServletRequest;

public class DeleteQuestion extends DeleteServlet<Question> {
    @Override
    public DAO<Question> getDao() {
        return ((DAOFactory) getServletContext().getAttribute("factory")).getQuestionDAO();
    }

    @Override
    protected String getPath(HttpServletRequest request) {
        return "/";
    }
}
