package com.abond.forum.controller.question;

import com.abond.forum.dao.base.QuestionDAO;
import com.abond.forum.entity.impl.Question;
import com.abond.forum.entity.impl.Topic;
import com.abond.forum.entity.impl.User;
import com.abond.forum.util.LogUtils;
import com.abond.forum.util.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Logger;

public class CreateQuestion extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(CreateQuestion.class.getSimpleName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.info("Create question");

        QuestionDAO questionDAO = (QuestionDAO) getServletContext().getAttribute("questionDAO");

        String name = ServletUtils.getStringParameter(request, "name");
        String message = ServletUtils.getStringParameter(request, "message");
        int topicId = ServletUtils.getIntegerParameter(request, "topicId", -1);
        int authorId = ServletUtils.getIntegerParameter(request, "authorId", -1);

        if (authorId < 0 || topicId < 0) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        User author = new User(authorId);
        Topic topic = new Topic(topicId);

        try {
            questionDAO.save(
                    new Question(
                            new Date(new java.util.Date().getTime()),
                            name,
                            message,
                            topic,
                            author
                    )
            );
        } catch (SQLException e) {
            LogUtils.logSevere(LOGGER, e);
        }

        response.sendRedirect("/questions/?t="+topicId);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
