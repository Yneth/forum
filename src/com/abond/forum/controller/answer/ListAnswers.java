package com.abond.forum.controller.answer;

import com.abond.forum.controller.base.ListController;
import com.abond.forum.dao.base.AnswerDAO;
import com.abond.forum.dao.base.QuestionDAO;
import com.abond.forum.entity.impl.Answer;
import com.abond.forum.entity.impl.Question;
import com.abond.forum.exception.ResourceNotFoundException;
import com.abond.forum.util.LogUtils;
import com.abond.forum.util.PaginationUtils;
import com.abond.forum.util.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

public class ListAnswers extends ListController {
    @Override
    protected void doProcessRequest(HttpServletRequest request, HttpServletResponse response, int from, int to)
            throws IOException, ResourceNotFoundException {
        AnswerDAO answerDAO = (AnswerDAO) getServletContext().getAttribute("answerDAO");
        QuestionDAO questionDAO = (QuestionDAO) getServletContext().getAttribute("questionDAO");

        int questionId = ServletUtils.getIntegerParameter(request, "q", -1);

        Collection<Answer> answers = null;
        Question question = null;
        try {
            answers = answerDAO.getPaginatedRelatedToQuestion(from, to, questionId);
            question = questionDAO.findById(questionId);
        } catch (ServletException e) {
            LogUtils.logSevere(LOGGER, e);
        }
        if (question == null) {
            throw new ResourceNotFoundException();
        }

        request.setAttribute("answers", answers);
        request.setAttribute("question", question);
    }

    @Override
    public String getResultPath() {
        return "/answers.jsp";
    }

    @Override
    public int getPageCount(HttpServletRequest request, int offset) {
        AnswerDAO answerDAO = (AnswerDAO) getServletContext().getAttribute("answerDAO");

        int questionId = ServletUtils.getIntegerParameter(request, "q", -1);

        int pageCount = 0;
        try {
            pageCount = PaginationUtils.getPageCount(answerDAO.getCountByQuestionId(questionId), offset);
        } catch (SQLException e) {
            LogUtils.logSevere(LOGGER, e);
        } catch (ServletException e) {
            LogUtils.logSevere(LOGGER, e);
        }
        return pageCount;
    }
}
