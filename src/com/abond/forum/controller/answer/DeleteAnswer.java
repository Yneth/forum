package com.abond.forum.controller.answer;

import com.abond.forum.controller.base.DeleteServlet;
import com.abond.forum.dao.base.AnswerDAO;
import com.abond.forum.dao.base.DAO;
import com.abond.forum.entity.impl.Answer;

import javax.servlet.http.HttpServletRequest;

public class DeleteAnswer extends DeleteServlet<Answer> {
    @Override
    public DAO<Answer> getDao() {
        return ((AnswerDAO)getServletContext().getAttribute("answerDAO"));
    }

    @Override
    protected String getPath(HttpServletRequest request) {
        return "/";
    }
}
