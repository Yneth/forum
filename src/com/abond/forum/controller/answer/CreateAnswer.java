package com.abond.forum.controller.answer;

import com.abond.forum.dao.base.AnswerDAO;
import com.abond.forum.entity.impl.Answer;
import com.abond.forum.entity.impl.Question;
import com.abond.forum.entity.impl.User;
import com.abond.forum.util.LogUtils;
import com.abond.forum.util.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Logger;

public class CreateAnswer extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(CreateAnswer.class.getSimpleName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.info("Create answer");

        AnswerDAO answerDAO = (AnswerDAO) getServletContext().getAttribute("answerDAO");

        String message = request.getParameter("message");
        int topicId = ServletUtils.getIntegerParameter(request, "topicId", -1);
        int authorId = ServletUtils.getIntegerParameter(request, "authorId", -1);
        int questionId = ServletUtils.getIntegerParameter(request, "questionId", -1);

        Date creationDate = new Date(new java.util.Date().getTime());


        if (authorId < 0 || questionId < 0) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        User author = new User(authorId);
        Question question = new Question(questionId);

        try {
            answerDAO.save(
                    new Answer(creationDate, message, question, author)
            );
        } catch (SQLException e) {
            LogUtils.logSevere(LOGGER, e);
        }

        response.sendRedirect("/questions/answers/?q="+questionId + "&t=" + topicId);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
