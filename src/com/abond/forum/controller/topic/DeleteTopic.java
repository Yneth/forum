package com.abond.forum.controller.topic;

import com.abond.forum.dao.base.DAO;
import com.abond.forum.dao.factory.DAOFactory;
import com.abond.forum.entity.impl.Topic;
import com.abond.forum.controller.base.DeleteServlet;

import javax.servlet.http.HttpServletRequest;

public class DeleteTopic extends DeleteServlet<Topic> {
    @Override
    public DAO<Topic> getDao() {
        return ((DAOFactory) getServletContext().getAttribute("factory")).getTopicDAO();
    }

    @Override
    protected String getPath(HttpServletRequest request) {
        return "/";
    }
}
