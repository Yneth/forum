package com.abond.forum.controller.topic;

import com.abond.forum.controller.base.ListController;
import com.abond.forum.dao.base.TopicDAO;
import com.abond.forum.entity.impl.Topic;
import com.abond.forum.util.LogUtils;
import com.abond.forum.util.PaginationUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.Collection;

public class ListTopic extends ListController {

    @Override
    protected void doProcessRequest(HttpServletRequest request, HttpServletResponse response, int from, int to) {
        TopicDAO topicDAO = ((TopicDAO) getServletContext().getAttribute("topicDAO"));
        Collection<Topic> topics = null;
        try {
            topics = topicDAO.getPaginatedEntities(from, to);
        } catch (ServletException e) {
            LogUtils.logSevere(LOGGER, e);
        } catch (SQLException e) {
            LogUtils.logSevere(LOGGER, e);
        }
        request.setAttribute("topics", topics);
    }

    @Override
    public String getResultPath() {
        return "/topics.jsp";
    }

    @Override
    public int getPageCount(HttpServletRequest request, int offset)
            throws ServletException, SQLException {
        TopicDAO topicDAO = ((TopicDAO) getServletContext().getAttribute("topicDAO"));
        return PaginationUtils.getPageCount(topicDAO.getCount(), offset);
    }
}
