package com.abond.forum.controller.topic;

import com.abond.forum.dao.base.TopicDAO;
import com.abond.forum.entity.impl.Topic;
import com.abond.forum.util.LogUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Logger;

public class CreateTopic extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(CreateTopic.class.getSimpleName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("creating a topic");
        String topicName = request.getParameter("name");
        if (topicName == null) {
            response.sendRedirect("/");
        }
        Topic question = new Topic(new Date(new java.util.Date().getTime()), topicName);
        try {
            ((TopicDAO) getServletContext().getAttribute("topicDAO")).save(question);
        } catch (SQLException e) {
            LogUtils.logSevere(LOGGER, e);
        }
        response.sendRedirect("/");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
