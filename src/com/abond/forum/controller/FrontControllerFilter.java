package com.abond.forum.controller;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Logger;

public class FrontControllerFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(FrontControllerFilter.class.getSimpleName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        String path = req.getRequestURI().substring(req.getContextPath().length());

        if (path.startsWith("/resources")) {
            chain.doFilter(request, response);
        }
        else {
            req.getRequestDispatcher("/forum" + req.getRequestURI()).forward(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
