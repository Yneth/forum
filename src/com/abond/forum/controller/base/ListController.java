package com.abond.forum.controller.base;

import com.abond.forum.exception.ResourceNotFoundException;
import com.abond.forum.util.LogUtils;
import com.abond.forum.util.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

public abstract class ListController<Type> extends HttpServlet {
    protected static final Logger LOGGER = Logger.getLogger(ListController.class.getSimpleName());
    private static final int DEFAULT_PAGE = 1;
    private static final int DEFAULT_OFFSET = 5;
    private static final String PAGE_PARAMETER = "p";
    private static final String OFFSET_PARAMETER = "o";

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int currentPage = ServletUtils.getIntegerParameter(request, PAGE_PARAMETER, DEFAULT_PAGE);
        int currentOffset = ServletUtils.getIntegerParameter(request, OFFSET_PARAMETER, DEFAULT_OFFSET);

        int from = currentOffset * (currentPage - 1);
        int to = from + currentOffset;

        int pageCount = 0;
        try {
            pageCount = getPageCount(request, currentOffset);

            doProcessRequest(request, response, from, to);
        } catch (SQLException e) {
            LogUtils.logSevere(LOGGER, e);
        } catch (ResourceNotFoundException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        if (currentPage > pageCount || currentPage <= 0) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        request.setAttribute("pageCount", pageCount);
        request.getRequestDispatcher(getResultPath()).forward(request, response);
    }

    protected abstract void doProcessRequest(HttpServletRequest request, HttpServletResponse response,
                                             int currentPage, int currentOffset)
            throws IOException, ResourceNotFoundException;

    public abstract String getResultPath();

    public abstract int getPageCount(HttpServletRequest request, int offset)
            throws ServletException, SQLException;
}
