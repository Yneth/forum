package com.abond.forum.controller.base;

import com.abond.forum.dao.base.DAO;
import com.abond.forum.util.LogUtils;
import com.abond.forum.util.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

public abstract class DeleteServlet<Entity> extends HttpServlet {
    private static final String ID_PARAMETER = "id";
    protected static final Logger LOGGER = Logger.getLogger(DeleteServlet.class.getSimpleName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = ServletUtils.getIntegerParameter(request, ID_PARAMETER, -1);
        if (id < 0) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        LOGGER.info("Deleting entity with id: " + id);
        DAO<Entity> dao = getDao();
        try {
            dao.deleteById(id);
        } catch (SQLException e) {
            LogUtils.logSevere(LOGGER, e);
        }
        LOGGER.info("successfully deleted");
        response.sendRedirect(getPath(request));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    public abstract DAO<Entity> getDao();
    protected abstract String getPath(HttpServletRequest request);
}
