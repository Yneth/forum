package com.abond.forum.config;

import com.abond.forum.dao.factory.DAOFactory;
import com.abond.forum.dao.factory.OracleDAOFactory;
import com.abond.forum.util.LogUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.sql.DataSource;
import java.util.logging.Logger;

@WebListener()
public class StartupListener implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {

    private static final String DATASOURCE_URL = "java:comp/env/oracle";
    private static final Logger LOGGER = Logger.getLogger(StartupListener.class.getSimpleName());

    // Public constructor is required by servlet spec
    public StartupListener() {
    }

    // -------------------------------------------------------
    // ServletContextListener implementation
    // -------------------------------------------------------
    public void contextInitialized(ServletContextEvent sce) {
      /* This method is called when the servlet context is
         initialized(when the Web application is deployed). 
         You can initialize servlet context related data here.
      */
        DataSource dataSource = getDataSource();

        DAOFactory daoFactory = new OracleDAOFactory(dataSource);

        ServletContext servletContext = sce.getServletContext();

        servletContext.setAttribute("userDAO", daoFactory.getUserDAO());
        servletContext.setAttribute("answerDAO", daoFactory.getAnswerDAO());
        servletContext.setAttribute("questionDAO", daoFactory.getQuestionDAO());
        servletContext.setAttribute("topicDAO", daoFactory.getTopicDAO());
    }

    private DataSource getDataSource() {
        DataSource dataSource = null;
        try {
            Context ic = new InitialContext();
            dataSource = (DataSource) ic.lookup(DATASOURCE_URL);
        } catch (NamingException e) {
            LOGGER.severe("Failed to initialize DataSource.");
            LogUtils.logSevere(LOGGER, e);
        }
        return dataSource;
    }

    public void contextDestroyed(ServletContextEvent sce) {
      /* This method is invoked when the Servlet Context
         (the Web application) is undeployed or
         Application Server shuts down.
      */
    }

    // -------------------------------------------------------
    // HttpSessionListener implementation
    // -------------------------------------------------------
    public void sessionCreated(HttpSessionEvent se) {
      /* Session is created. */
    }

    public void sessionDestroyed(HttpSessionEvent se) {
      /* Session is destroyed. */
    }

    // -------------------------------------------------------
    // HttpSessionAttributeListener implementation
    // -------------------------------------------------------

    public void attributeAdded(HttpSessionBindingEvent sbe) {
      /* This method is called when an attribute
         is added to a session.
      */
    }

    public void attributeRemoved(HttpSessionBindingEvent sbe) {
      /* This method is called when an attribute
         is removed from a session.
      */
    }

    public void attributeReplaced(HttpSessionBindingEvent sbe) {
      /* This method is invoked when an attibute
         is replaced in a session.
      */
    }
}
