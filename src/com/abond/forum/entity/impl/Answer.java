package com.abond.forum.entity.impl;

import com.abond.forum.entity.base.AbstractEntity;

import java.sql.Date;

public class Answer extends AbstractEntity {
    private String message;
    private Question question;
    private User author;

    public Answer() {
    }

    public Answer(Integer id, Date creationDate, String message, Question question, User author) {
        super(id, creationDate);
        this.message = message;
        this.question = question;
        this.author = author;
    }

    public Answer(Date creationDate, String message, Question question, User author) {
        super(creationDate);
        this.message = message;
        this.question = question;
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Answer answer = (Answer) o;

        if (!author.equals(answer.author)) return false;
        if (!message.equals(answer.message)) return false;
        if (!question.equals(answer.question)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + message.hashCode();
        result = 31 * result + question.hashCode();
        result = 31 * result + author.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Answer{");
        sb.append("message='").append(message).append('\'');
        sb.append(", question=").append(question);
        sb.append(", author=").append(author);
        sb.append('}');
        return sb.toString();
    }
}
