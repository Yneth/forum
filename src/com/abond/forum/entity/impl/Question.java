package com.abond.forum.entity.impl;

import com.abond.forum.entity.base.AbstractEntity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

public class Question extends AbstractEntity {
    private String name;
    private String message;
    private User author;
    private Topic topic;
    private Collection<Answer> answers;
    private int answersCount;

    public Question() {
    }

    public Question(Integer id) {
        super(id);
    }

    public Question(Date date, String name, String message, Topic topic, User author) {
        super(date);
        this.name = name;
        this.message = message;
        this.topic = topic;
        this.author = author;
    }

    public Question(Integer id, Date dateCreation,
                    String name, String message,
                    Topic topic, User author,
                    int answersCount) {
        super(id, dateCreation);
        this.name = name;
        this.topic = topic;
        this.message = message;
        this.author = author;
        this.answersCount = answersCount;
        this.answers = new ArrayList<Answer>(answersCount);
    }

    public Question(Integer id, Date dateCreation,
                    String name, String message,
                    Topic topic, User author,
                    Collection<Answer> answers) {
        super(id, dateCreation);
        this.name = name;
        this.topic = topic;
        this.message = message;
        this.author = author;
        this.answersCount = answers.size();
        this.answers = answers;
    }

    public Question(int questionId, Date creationDate, String name, String message, Topic topic, User user) {
        super(questionId, creationDate);
        this.name = name;
        this.message = message;
        this.topic = topic;
        this.author = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Collection<Answer> answers) {
        this.answers = answers;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public int getAnswersCount() {
        return answersCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Question question = (Question) o;

        if (answersCount != question.answersCount) return false;
        if (answers != null ? !answers.equals(question.answers) : question.answers != null) return false;
        if (!author.equals(question.author)) return false;
        if (!message.equals(question.message)) return false;
        if (!name.equals(question.name)) return false;
        if (!topic.equals(question.topic)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + message.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + topic.hashCode();
        result = 31 * result + (answers != null ? answers.hashCode() : 0);
        result = 31 * result + answersCount;
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Question{");
        sb.append("name='").append(name).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", author=").append(author);
        sb.append(", topic=").append(topic);
        sb.append(", answers=").append(answers);
        sb.append(", answersCount=").append(answersCount);
        sb.append('}');
        return sb.toString();
    }
}
