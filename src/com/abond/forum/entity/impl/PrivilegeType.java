package com.abond.forum.entity.impl;

public enum PrivilegeType {
    BANNED(0), USER(1), ADMIN(2);

    private final int type;

    PrivilegeType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static PrivilegeType byId(int id) {
        PrivilegeType[] values = PrivilegeType.values();
        for (PrivilegeType type : values)
            if (type.getType() == id)
                return type;
        return BANNED;
    }

}
