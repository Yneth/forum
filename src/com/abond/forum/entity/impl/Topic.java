package com.abond.forum.entity.impl;

import com.abond.forum.entity.base.AbstractEntity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

public class Topic extends AbstractEntity {
    private String name;
    private int questionCount;
    private Collection<Question> questions;

    public Topic() {
    }

    public Topic(Integer topicId) {
        super(topicId);
    }

    public Topic(int topicId, String name) {
        super(topicId);
        this.name = name;
        questionCount = 0;
    }

    public Topic(Integer id, Date creationDate, String name) {
        super(id, creationDate);
        this.name = name;
    }

    public Topic(Integer id, Date creationDate, String name, int questionCount) {
        super(id, creationDate);
        this.name = name;
        this.questions = new ArrayList<Question>(questionCount);
        this.questionCount = questionCount;
    }

    public Topic(Integer id, Date creationDate, String name, Collection<Question> questions) {
        super(id, creationDate);
        this.name = name;
        this.questions = new ArrayList<Question>(questionCount);
        this.questionCount = 0;
    }

    public Topic(Date date, String topicName) {
        super(date);
        this.name = topicName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Collection<Question> questions) {
        this.questions = questions;
    }

    public int getQuestionCount() {
        return questionCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Topic topic = (Topic) o;

        if (!name.equals(topic.name)) return false;
        if (questions != null ? !questions.equals(topic.questions) : topic.questions != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (questions != null ? questions.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Topic{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
