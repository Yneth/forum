package com.abond.forum.entity.impl;

import com.abond.forum.entity.base.AbstractEntity;

import java.sql.Date;
import java.util.Collection;

public class User extends AbstractEntity {
    private String username;
    private String password;
    private String firstname;
    private String secondname;
    private PrivilegeType privilege;
    private Collection<Question> questions;
    private Collection<Answer> answers;

    public User(Integer id) {
        super(id);
    }

    public User(Integer id, String username) {
        super(id);
        this.username = username;
    }

    public User(Integer id, Date dateCreation, String username,
                String password, String firstname, String secondname,
                PrivilegeType privilege) {
        super(id, dateCreation);
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.secondname = secondname;
        this.privilege = privilege;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public PrivilegeType getPrivilege() {
        return privilege;
    }

    public void setPrivilege(PrivilegeType privilege) {
        this.privilege = privilege;
    }

    public Collection<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Collection<Question> questions) {
        this.questions = questions;
    }

    public Collection<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Collection<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        User user = (User) o;

        if (firstname != null ? !firstname.equals(user.firstname) : user.firstname != null) return false;
        if (!password.equals(user.password)) return false;
        if (privilege != user.privilege) return false;
        if (secondname != null ? !secondname.equals(user.secondname) : user.secondname != null) return false;
        if (!username.equals(user.username)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (secondname != null ? secondname.hashCode() : 0);
        result = 31 * result + privilege.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("username='").append(username).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", secondname='").append(secondname).append('\'');
        sb.append(", privilege=").append(privilege);
        sb.append('}');
        return sb.toString();
    }
}
